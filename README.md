#Simple Daily Mysql Backup Script

	A simple shell script for automating scheduled mysql dumps
	
##Requirements
--------------
* mysqldump
* directory for storing backups

##Configuration
---------------

**edit the bolded fields**

* mysql_user=**root**

* mysql_password=**root**

* backupDir=**/path/to/save/mysql/dumps/**

* days=**31**   `number of days to keep backups in backupDir`

* databases=**'--all-databases'**   `either --all-databases or single database name to backup`

* file_prefix=**'bkup-'**  `file prefix for stored backups`



##How-To
--------

[Optional] Install crontab ie. daily at midnight:

>00 00 * * * /path/to/mysql_backup.sh

or

[Run Manually]

>./path/to/mysql_backup.sh