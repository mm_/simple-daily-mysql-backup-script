#!/bin/bash -x

mysql_user=root
mysql_password=root
backupDir=/path/to/mysql_dumps/
days=31
databases='--all-databases'
file_prefix='bkup-'

myDate=$(date '+%Y-%m-%d')
dbName=$file_prexix$myDate.sql.gz
cd $backupDir
mysqldump -u$mysql_user -p\'$mysql_password\' $databases | gzip > $dbName
 
#delete anything older than $days  days
find $backupDir -mtime +$days | xargs rm -f

